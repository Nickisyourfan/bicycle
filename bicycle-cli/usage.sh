#!/bin/bash

usage(){
  fmt_help="  %-20s\t%-54s\n"
  echo An unofficial cli for Cycle.io. '(v1.3)'
  echo
  echo "Usage: bicycle <command>"
  echo
  echo Commands:
  printf "${fmt_help}" \
    "init" "creates an encrypted .bicycle file in the root of your project" \
    "init -g" "globally stores your cycle credentials for use with other projects" \
    "deploy" "builds, pushes, uploads, and reimages cycle container" \
    "ssh" "instant two way console into a running instance" \
    "pipeline" "lists and triggers pipeline of your choosing" \
    "console" "instantly view the console of a running instance" \
    "announcements" "show the announcements related to a the hub" \
    "help" "shows this dialogue."
  echo
}
