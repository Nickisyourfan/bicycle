#!/bin/bash

fetch_container() {
  echo Fetching Container...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$container_id"

  #Get the Container Information from Cycle
  container=$(curl https://api.cycle.io/v1/containers/"$container_id"'?meta=domain' \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    --silent \
    | jq '.data'
  )
  eval "$(jq -r '
    "container_id=\(.id|@sh);
    container_name=\(.name|@sh);
    image_id=\(.image.id|@sh);
    environment_id=\(.environment.id|@sh);
    cluster=\(.environment.cluster|@sh);
    container_domain=\(.meta.domain|@sh);
    domains=\(.meta.domains|@sh);"
    ' <<< "$container")"
}

reimage_container() {
  echo Reimaging Container...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$container_id" "$image_id"

  reimage=$(curl https://api.cycle.io/v1/containers/"$container_id"/tasks  \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    -H 'Content-Type: application/json' \
    -d '{"action":"reimage","contents":{"image_id":"'$image_id'"}}' \
    -X POST \
    --silent \
    | jq '.data' 
  )
  eval "$(jq -r '
    "job_id=\(.job_id|@sh);"
    ' <<< "$reimage")"
}

create_container() {
  echo Creating Container...
  
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$container_name" "$environment_id" "$image_id"


  container=$(curl https://api.cycle.io/v1/containers \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    -H 'Content-Type: application/json' \
    -d '{"name":"'"$container_name"'","stateful":true,"environment_id":"'"$environment_id"'","image_id":"'"$image_id"'","config":{"network":{"public":"enable","hostname":"'"$container_name"'"},"deploy":{"instances":1,"constraints":{"node":{"tags":{"any":[],"all":[]}},"secrets":[],"containers":[]}},"integrations":{"lets_encrypt":{"enable":false}}}}' \
    -X POST \
    --silent \
    | jq '.data'
  )
  eval "$(jq -r '
    "container_id=\(.id|@sh);
    container_name=\(.name|@sh)
    image_id=\(.image.id|@sh)
    environment_id=\(.environment.id|@sh)
    cluster=\(.environment.cluster|@sh)"
    ' <<< "$container")"
}

start_container() {
  echo Starting container...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$container_id" 

  starting_container=$(curl https://api.cycle.io/v1/containers/"$container_id"/tasks \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    -H 'Content-Type: application/json' \
    -d '{"action":"start"}' \
    -X POST \
    --silent \
    | jq '.data'
  )

  eval "$(jq -r '
    "job_id=\(.job_id|@sh);"
    ' <<< "$starting_container")"
  echo
}

set_container_name() {
  echo Choose a container name:
  read container_name
}
