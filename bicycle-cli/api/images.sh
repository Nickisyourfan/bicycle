#!/bin/bash

fetch_image() {
  echo Fetching Image...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$image_id"

  #Get The Image Source Information
  image=$(curl https://api.cycle.io/v1/images/"$image_id" \
    -H 'Authorization: Bearer '$cycle_api_key \
    -H 'X-Hub-Id: '$cycle_hub_id \
    --silent \
    | jq '.data' 
  )

  eval "$(jq -r '
    "image_id=\(.id|@sh);
    source_id=\(.source|.details|.id|@sh);
    source_type=\(.source|.details|.origin|.type|@sh);
    source_target=\(.source|.details|.origin|.details|.target|@sh)
    source_location=\(.source|.details|.origin|.details|.url|@sh);"
    ' <<< "$image")"
}

create_image() {
  echo Creating Image...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$source_id"

  image=$(curl https://api.cycle.io/v1/images \
    -H 'Authorization: Bearer '$cycle_api_key \
    -H 'X-Hub-Id: '$cycle_hub_id \
    -H 'Content-Type: application/json' \
    -d '{"source_id":"'"$source_id"'"}' \
    -X POST \
    --silent \
    | jq '.data'
  )

  eval "$(jq -r '
    "image_id=\(.id|@sh);"
    ' <<< "$image")"
}

import_image() {
  echo Importing Image...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$image_id"

  imported_image=$(curl https://api.cycle.io/v1/images/"$image_id"/tasks \
    -H 'Authorization: Bearer '$cycle_api_key \
    -H 'X-Hub-Id: '$cycle_hub_id \
    -H "Content-Type: application/json" \
    -d '{"action":"import"}' \
    -X POST \
    --silent \
    | jq '.data'
  )
  eval "$(jq -r '
    "job_id=\(.job_id|@sh);"
    ' <<< "$imported_image")"
}
