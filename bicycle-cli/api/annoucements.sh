#!/bin/bash

list_annoucements() {
  echo Listing announcements...
  validate_vars "$cycle_api_key" "$cycle_hub_id"

  announcements=$(curl https://api.cycle.io/v1/announcements \
    -H 'Authorization: Bearer '$cycle_api_key \
    -H 'X-Hub-Id: '$cycle_hub_id \
    --silent \
    | jq '.data'
  )
}
