#!/bin/bash

fetch_clusters() {
  echo Fetching clusters...
  validate_vars "$cycle_api_key" "$cycle_hub_id"

  clusters=$(curl  https://api.cycle.io/v1/infrastructure/servers/clusters \
    -H 'Authorization: Bearer '$cycle_api_key \
    -H 'X-Hub-Id: '$cycle_hub_id \
    --silent \
    | jq '.data' 
  )
}

pick_cluster() {
  validate_vars "$clusters"

  cluster_count=$(echo "$clusters" | jq length)

  if [ "$cluster_counter" = 0 ]; then
    echo You do not have clusters availbale to cycle.
    echo Please sign on and add clusters before using Bicycle.
    echo
    exit
  fi

  echo Clusters:
  for((i = 0 ; i < "$cluster_count" ; i++)); do
    cluster_name=$(echo "$clusters" | jq '.['"$i"']' )
    echo "$i". "$cluster_name"
  done
  echo
  echo Choose cluster. [ By the number ]
  read cluster_choice

  if [ "$cluster_choice" -le "$cluster_count" ]; then
    cluster=$(echo "$clusters" | jq -r '.['"$cluster_choice"']')
  fi
  echo
}
