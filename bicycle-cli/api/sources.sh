#!/bin/bash

list_sources() {
  echo Listing sources...
  validate_vars "$cycle_api_key" "$cycle_hub_id"
  
  sources=$(curl https://api.cycle.io/v1/images/sources \
      -H 'Authorization: Bearer '$cycle_api_key \
      -H 'X-Hub-Id: '$cycle_hub_id \
      --silent \
      | jq '.data'
    )
}

fetch_source() {
  echo Fetching source...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$source_id"

  source=$(curl https://api.cycle.io/v1/images/sources/"$source_id" \
      -H 'Authorization: Bearer '$cycle_api_key \
      -H 'X-Hub-Id: '$cycle_hub_id \
      --silent \
      | jq '.data'
    )
  eval "$(jq -r '
    "source_id=\(.id|@sh);"
    ' <<< "$source")"
}

create_source_docker_hub() {
  echo Creating source from docker hub...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$source_type" "$source_target" "$source_name"

  source=$(curl https://api.cycle.io/v1/images/sources \
      -H 'Authorization: Bearer '$cycle_api_key \
      -H 'X-Hub-Id: '$cycle_hub_id \
      -H 'Content-Type: application/json' \
      -d '{"name":"'$source_name'","type":"direct","origin":{"type":"'"$source_type"'","details":{"target":"'"$source_location"'/'"$source_target"'"}}}' \
      -X POST \
      --silent \
      | jq '.data'
  )

  echo "$source"

  eval "$(jq -r '
    "source_id=\(.id|@sh);"
    ' <<< "$source")"
}

create_source_docker_registry() {
  echo Creating source from docker registry...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$source_type" "$source_target" "$username" "$password" "$source_location"

  source=$(curl https://api.cycle.io/v1/images/sources \
      -H 'Authorization: Bearer '$cycle_api_key \
      -H 'X-Hub-Id: '$cycle_hub_id \
      -H "Content-Type: application/json" \
      -d '{"name":"'"$source_name"'", "type": "direct", "origin":{"type": "'"$source_type"'", "details": {"target": "'"$source_target"'", "username": "'$username'", "password": "'$password'", "url": "'$source_location'"}}}' \
      -X POST \
      --silent \
      | jq '.data'
  )
  
  eval "$(jq -r '
    "source_id=\(.id|@sh);"
    ' <<< "$source")"
}

set_source_target() {
  echo Project Name and Tag:
  echo Ex: node:apline
  read source_target
  echo
}

set_source_location() {
  echo Enter Project Location - This is either your username or registry url.
  echo
  echo Docker Hub Ex: user_name
  echo Docker Registry Ex: registry.my_company.com
  echo
  read source_location
  echo
}

name_new_source() {
  echo Set Cycle Source Name. This is a reusable identifyer for your image location: 
  read source_name
  echo
}

set_source_type() {
  echo Select where the source comes from. [ select number ]
  echo 1. Docker Hub
  echo 2. Docker Registry.
  read source_type
  if [ "$source_type" = 1 ]; then
    echo "Chose Docker Hub"
    source_type="docker-hub"
  elif [ "$source_type" = 2 ]; then
    echo "Chose Docker Registry"
    source_type="docker-registry"
  else
    echo This choice was invalid. Exiting...
    exit.
  fi
  echo
}

pick_source_by_name() {
  validate_vars "$sources"

  number_of_sources=$(echo "$sources" | jq length)

  if [ "$number_of_sources" = 0 ]; then 
    echo You do not have any image soures.
    echo
    exit
  fi

  echo Sources:
  for((i = 0 ; i < "$number_of_sources" ; i++)); do
    source_name=$(echo "$sources" | jq '.['"$i"']' | jq '.name')
    echo "$i". "$source_name"
  done

  echo Which source? [ Choose by number. ]
  read source_choice
  if [ "$source_choice" -le "$number_of_sources" ]; then
    source_id=$(echo "$sources" | jq '.['"$source_choice"']' | jq -r '.id')
  else
    echo This was not a valid choice... exiting.
    exit
  fi
}
