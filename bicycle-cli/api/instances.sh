#!/bin/bash

list_instances() {
  echo Getting Instances...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$container_id"

  instances=$(curl https://api.cycle.io/v1/containers/"$container_id"/instances \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    --silent \
  | jq '.data'
  )
  instance_count=$(echo "$instances" | jq length )
}

get_instance_choice() {
  validate_vars "$instance_count"

  if [ "$instance_count" -gt 1 ]; then
    echo Select an instance. [enter the number of the instance]
    read instance_choice
  else
    instance_choice=1
  fi
  
  instance_choice_index=$(( $instance_choice - 1 ))

  if [ "$instance_choice" -le "$instance_count" ]; then
    instance=$(echo "$instances" | jq '.['"$instance_choice_index"']')
    instance_id=$(echo "$instance" | jq -r '.id')
  else
    echo This number instance does not exist. 
  fi
}

get_instance() {
  echo Getting Instance...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$container_id" "$instance_id"

  instance=$(curl  https://api.cycle.io/v1/containers/"$container_id"/instances/"$instance_id" \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    | jq '.data'
  )

  eval "$(jq -r '
    "instance_id=\(.id|@sh);"
    ' <<< "$instance")"
}

get_ssh_credentials() {
  echo Getting SSH Credentials...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$container_id" "$instance_id"

  ssh_credentials=$(curl https://api.cycle.io/v1/containers/"$container_id"/instances/"$instance_id"/ssh \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    --silent \
    | jq '.data'
  )
  ssh_address_port=$(echo "$ssh_credentials" | jq -r '.address')
  ssh_address="${ssh_address_port%:22}"
  ssh_secret=$(echo "$ssh_credentials" | jq -r '.secret')
}

expire_ssh_tokens() {
  echo Expiring SSH Tokens...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$container_id" "$instance_id"

  curl https://api.cycle.io/v1/containers/"$container_id"/instances/"$instance_id"/ssh \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    -X DELETE \
    --silent
  echo
  echo Alert: ssh token expired on exit.
}

get_console_credentials() {
  echo Getting Console Credentials...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$container_id" "$instance_id"

  console_credentials=$(curl  https://api.cycle.io/v1/containers/"$container_id"/instances/"$instance_id"/console \
      -H 'Authorization: Bearer '"$cycle_api_key" \
      -H 'X-Hub-Id: '"$cycle_hub_id" \
      --silent \
      | jq '.data'
    )
  eval "$(jq -r '
    "console_token=\(.token|@sh);
    console_address=\(.address|@sh)"
    ' <<< "$console_credentials")"
}

request_console_websocket() {
  echo Requesting Console Websocket...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$console_token"
  
  curl  -o - --http1.1 --include \
    --no-buffer \
    --header 'Connection: Upgrade' \
    --header 'Upgrade: websocket' \
    --header 'Host: api.cycle.io' \
    --header 'Origin: https://api.cylcle.io' \
    --header 'Sec-WebSocket-Key: '"$cycle_api_key" \
    --header 'Sec-WebSocket-Version: 13' \
    -H 'x-cycle-token: '"$console_token" \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    https://console.cycle.io:8920/v1/console?token="$console_token"
}
