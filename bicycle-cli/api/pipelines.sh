#!/bin/bash

list_pipelines() {
  echo Listing pipelines...
  validate_vars "$cycle_api_key" "$cycle_hub_id"

  pipelines=$(curl https://api.cycle.io/v1/pipelines \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    --silent \
    | jq '.data'
  ) 
}

fetch_pipeline() {
  echo Fetching pipeline...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$pipeline_id"

  pipeline=$(curl https://api.cycle.io/v1/pipelines/"$pipeline_id" \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    --silent \
    | jq '.data'
  ) 

  eval "$(jq -r '
    "pipeline_id=\(.id|@sh);
    pipeline_name=\(.name|@sh)"
    ' <<< "$pipeline")"
}

trigger_pipeline() {
  echo Triggering pipeline...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$pipeline_id"

  trigger=$(curl https://api.cycle.io/v1/pipelines/"$pipeline_id"/tasks \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    -H "Content-Type: application/json" \
    -d '{"action":"trigger"}' \
    -X POST \
    --silent \
    | jq '.data'
  )

  eval "$(jq -r '
    "job_id=\(.job_id|@sh);"
    ' <<< "$trigger")"
}
