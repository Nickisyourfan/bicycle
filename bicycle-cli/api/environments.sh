#!/bin/bash

list_environments () {
  echo Listing Environments...
  validate_vars "$cycle_api_key" "$cycle_hub_id"

  environments=$(curl -g 'https://api.cycle.io/v1/environments?sort=name&page[number]=1&page[size]=99' \
    -H 'Authorization: Bearer '"$cycle_api_key" \
    -H 'X-Hub-Id: '"$cycle_hub_id" \
    --silent \
    | jq '.data' 
  )
}

fetch_environment() {
  echo Fetching Environment...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$environment_id"

  environment=$(curl 'https://api.cycle.io/v1/environments/'"$environment_id"'?meta=containers' \
    -H 'Authorization: Bearer '$cycle_api_key \
    -H 'X-Hub-Id: '$cycle_hub_id \
    --silent \
    | jq '.data' 
  )
  echo
}

create_environment() {
  echo Creating Environment...
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$cluster" "$environment_name" "$legacy_networking"

  environment=$(curl https://api.cycle.io/v1/environments \
    -H 'Authorization: Bearer '$cycle_api_key \
    -H 'X-Hub-Id: '$cycle_hub_id \
    -H 'Content-Type: application/json' \
    -d '{"name":"'$environment_name'","cluster":"'"$cluster"'","about":{"description":"New environment made with bicycle."},"features":{"legacy_networking":'"$legacy_networking"'}}' \
    -X POST \
    --silent \
    | jq '.data'
  )
 
  eval "$(jq -r '
    "environment_id=\(.id|@sh);"
    ' <<< "$environment")"

  echo
}

choose_environment_name() {
  echo Choose environment name:
  read environment_name
  echo
}

choose_environment_ipv() {
  echo Use Ipv6? If no, then use legacy networking. [ y/n ]
  read environment_ipv
  if [ "$environment_ipv" = 'yes' ] || [ "$environment_ipv" = 'y' ]; then
    legacy_networking=false
  else
    legacy_networking=true
  fi
  echo
}

pick_environment_by_name() {
  validate_vars "$environments"

  number_of_environments=$(echo "$environments" | jq length)

  if [ "$number_of_environments" -eq 0 ]; then
    echo It seems you have no environments.
    echo Restart the init process, and select to create environment.
    exit
  fi

  echo Environments:
  for((i = 0 ; i < "$number_of_environments" ; i++)); do
    environment_name=$(echo "$environments" | jq '.['"$i"']' | jq '.name')
    echo "$i". "$environment_name"
  done
  echo

  echo Which environment? [ Choose by number. ]
  read environment_choice
  if [ "$environment_choice" -le "$number_of_environments" ]; then
    environment_id=$(echo "$environments" | jq '.['"$environment_choice"']' | jq -r '.id')
  else
    echo This was not a valid choice... exiting.
    exit
  fi
  echo
}

pick_container_from_environment() {
  validate_vars "$environment"
  
  number_of_containers_in_environment=$(echo "$environment" | jq '.meta.containers' | jq length )

  if [ -z "$number_of_containers_in_environment" ]; then
    echo There are no containers in this environment.
    echo Restart the init process, and select project is not already deployed to cycle to create a container.
    exit
  fi

  echo Containers:
  for((i = 0 ; i < "$number_of_containers_in_environment" ; i++)); do
    container_name=$(echo "$environment" | jq '.meta.containers' | jq '.['"$i"']' | jq '.name' )
    echo "$i". "$container_name"
  done
  echo

  echo Which container? [ Choose by number ]
  read container_choice
  if [ "$container_choice" -le "$number_of_containers_in_environment" ] && [ "$container_choice" -ge 0 ]; then
    container=$(echo "$environment" | jq '.meta.containers' | jq '.['"$container_choice"']')
    container_id=$(echo "$container" | jq -r '.id')
    image_id=$(echo "$container" | jq -r '.image.id')
  else
    echo This was not a valid choice... exiting. 
    exit
  fi
  echo
}
