#!/bin/bash

fetch_job() {
  validate_vars "$cycle_api_key" "$cycle_hub_id" "$job_id"

  job=$(curl https://api.cycle.io/v1/jobs/"$job_id"  \
    -H 'Authorization: Bearer '$cycle_api_key \
    -H 'X-Hub-Id: '$cycle_hub_id \
    --silent \
    | jq '.data'
  )
  eval "$(jq -r '
    "job_id=\(.id|@sh);
    job_state_current=\(.state.current|@sh)"
    ' <<< "$job")"
}

track_current_job() {
  echo Tracking progress of job: "$job_id"...
  validate_vars "$job_id"

  finished=false
  check_if_job_finished() {
    #Get the job status
    fetch_job

    #Echo Job Status To Console
    if [ "$job_state_current" = "running" ]
    then
      echo --Working...
    fi

    if [ "$job_state_current" = "completed" ]
    then
      echo --Succesfully finished job, "$job_id"
      finished=true
    fi

    if [ "$job_state_current" != "running" ] && [ "$job_state_current" != "completed" ];
    then
      echo Uh Oh - Something went wrong with the job, "$job_id".
      echo The job returned with a status of: "$job_state_current".
      exit
    fi

    sleep 10
    
  }

  while [ "$finished" == "false" ]
    do check_if_job_finished 
    done

}
