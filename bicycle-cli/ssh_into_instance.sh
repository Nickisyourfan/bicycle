#!/bin/bash

ssh_into_instance() {
  #Check to make sure container id exists
  require_project_vars

  #Get all instances and number of instances
  list_instances

  #Prompt user to choose the instance to ssh into.
  echo Instances available: "$instance_count"
  get_instance_choice

  #Fetch the ssh credentials using the id of the choice.
  get_ssh_credentials

  #Init ssh session
  sshpass -p "$ssh_secret" ssh "$ssh_address"

  #Expire tokens upon exit
  expire_ssh_tokens

} 

