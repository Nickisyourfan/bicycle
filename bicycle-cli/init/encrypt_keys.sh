#!/bin/bash

encrypt_key() {
  echo "$1" | openssl enc -aes-256-cbc -a -salt -iter 29 -pass pass:"$project_passcode"
}
