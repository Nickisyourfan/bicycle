#!/bin/bash

get_cycle_api_key() {

  if [ ! "$cycle_api_key" ]; then
    new_api_key=true
  fi

  get_cycle_key(){
    echo Cycle API Key:
    read -s cycle_api_key 
    eval_cycle_key
  }
  
  eval_cycle_key(){
    cycle_key_count=${#cycle_api_key}
    if [ -n "$cycle_api_key" ] && [ "$cycle_key_count" -ne 67 ]; then
      echo ERROR: Incorrect length. Cycle API Keys are 67 characters long, without spaces. 
      get_cycle_key
    elif [ -z "$cycle_api_key" ]; then
      get_cycle_key
    elif [ -n "$cycle_api_key" ] && [ -z "$new_api_key" ] && [ -z "$use_global" ]; then
      echo Api Key is currently set. Set New Key? [ y/n ]
      read new_api_key
      if [ "$new_api_key" = "yes" ] || [ "$new_api_key" = "y" ]; then
        get_cycle_key
      else
        echo Keeping the previous API key...
      fi
    elif [ -n "$cycle_api_key" ] && [ -z "$new_api_key" ] && [ -n "$use_global" ]; then
      echo Using globally set API Key...
    fi
    echo
  }
  
  eval_cycle_key
}
