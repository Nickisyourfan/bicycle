#!/bin/bash

get_cycle_hub_id() {
  if [ ! "$cycle_hub_id" ]; then
    new_hub_id=true
  fi
  
  get_hub_id(){
    echo Cycle Hub ID:
    read -s cycle_hub_id
    eval_hub_id
  }
  
  eval_hub_id(){
    hub_id_count=${#cycle_hub_id}
    if [ -n "$cycle_hub_id" ] && [ "$hub_id_count" -ne 24 ]; then
      echo Hub Ids are 24 characters long, without spaces. 
      get_hub_id
    elif [ -z "$cycle_hub_id" ]; then
      get_hub_id
    elif [ -n "$cycle_hub_id" ] && [ -z "$new_hub_id" ] && [ -z "$use_global" ]; then
      echo Hub Id is currently set. Set New Hub Id? [ y/n ]
      read new_hub_id
      if [ "$new_hub_id" = "yes" ] || [ "$new_hub_id" = "y" ];
      then
        get_hub_id
      else
        echo Keeping the previous hub id...
      fi
    elif [ -n "$cycle_hub_id" ] && [ -z "$new_hub_id" ] && [ -n "$use_global" ]; then
      echo Using globally set Hub Id...
    fi
    echo
  }

  eval_hub_id
}
