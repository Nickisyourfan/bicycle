#!/bin/bash

check_project_status () {
  echo Project is already on cycle? [ y/n ]
  read project_status
  echo
  if [ "$project_status" = "yes" ] || [ "$project_status" = "y" ]; then
    echo Accessing existing Cycle Project.
    creating_new_project=false
  else
    echo Creating new deployment.
    creating_new_project=true
  fi
  echo
}
