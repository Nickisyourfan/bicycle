#!/bin/bash

get_source_location() {

  if [ ! "$source_location" ];
  then
    relocate_project=true
  fi
  
  get_location(){
    echo Dockerhub Username Location or Registry Link:
    read source_location
    eval_source_location
  }
  
  eval_source_location(){
    source_location_word_count=$(echo "$source_location" | wc -w)
    if [ -n "$source_location" ] && [ "$source_location_word_count" -gt 1 ];
    then
      echo ERROR: No spaces. Docker Hub username or Private Registry Link.
      get_location
    elif [ -z "$source_location" ]
    then
      get_location
    elif [ -n "$source_location" ] && [ -z "$relocate_project" ];
    then
      echo Current Project Location: "$source_location". Relocate? [ y/n ]
      read relocate_project
      if [ "$relocate_project" = "yes" ] || [ "$relocate_project" = "y" ];
      then
        get_location
      else
        echo Keeping the previous location...
      fi
    fi
  }
  eval_source_location
}
