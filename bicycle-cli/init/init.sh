#!/bin/bash

create_init_file() {

  #Set Project Passcode
  set_project_passcode

  #Get Cycle Api Key
  get_cycle_api_key

  #Get Hub ID
  get_cycle_hub_id

  #Check if project needs to be made from scratch
  check_project_status

  #Switch the script if project needs to be made from scratch
  case "$creating_new_project" in
    false)
      #List Environments to pick from
      list_environments
      #Have user pick an environment
      pick_environment_by_name
      #Fetch the environment
      fetch_environment
      #Pick container from this environment
      pick_container_from_environment
      #Get Registry Credentials
      get_user_registry_credentials
      ;;
    true)
      echo Create new environment? [ y/n ]
      read create_new_environment
      echo
      #Check if user wants to create a new environment.
      if [ "$create_new_environment" = "yes" ] || [ "$create_new_environment" = "y" ]; then
        #First, get the clusters.
        fetch_clusters
        #Have user pick which cluster to use. 
        pick_cluster
        #Name the environment
        choose_environment_name
        #Choose ipv
        choose_environment_ipv
        #Create the environment
        create_environment
      else
        #List Environments to pick from
        list_environments
        #Have user pick an environment
        pick_environment_by_name
        #Fetch the environment
        fetch_environment
      fi

      #Check what image to use. 
      echo Upload a new image to cycle? [ y/n ]
      read upload_new_image_choice
      echo
      if [ "$upload_new_image_choice" = 'yes' ] || [ "$upload_new_image_choice" = "y" ]; then
        #Create Source
        name_new_source
        set_source_type
        echo "$source_type"
        if [ "$source_type" = "docker-registry" ]; then
          get_user_registry_credentials
          set_source_target
          set_source_location
          create_source_docker_registry
        else
          get_user_registry_credentials
          set_source_target
          set_source_location
          create_source_docker_hub
        fi
        #Check for Prod Dockerfile
        if test -r ./Dockerfile.prod; then 
          echo Do you want to use Dockerfile.prod to build?
          read use_prod
        fi
        #If Prod file, and user says yes, then use prod file.
        if [ "$use_prod" = 'y' ] || [ "$use_prod" = "yes" ]; then
          docker_file="Dockerfile.prod"
        else
          docker_file="Dockerfile"
        fi
        #Build new image to registry/hub
        sudo docker build -t "$source_location"'/'"$source_target" . -f "$docker_file"
        #Push to existing registry
        sudo docker push "$source_location"'/'"$source_target"
        #Create Image
        create_image
        #Import Image
        import_image
        #Confirm Image Gets Uploaded
        track_current_job
      else
        #List all sources
        list_sources
        #Pick a source
        pick_source_by_name
        #Create Image
        create_image
        import_image
        track_current_job
      fi

      #Set Container Name
      set_container_name
      #Create new container
      create_container

      #Optionally start container
      echo Do you want to start this container? [ y/n ]
      read should_start_container
      if [ "$should_start_container" = "y" ] || [ "$should_start_container" = "yes" ]; then
        start_container
        #Confirm Container Starts
        track_current_job
      fi
      ;;
    *)
      echo Error reading choice...
      exit
    esac

  #Encrypt keys before saving
  cycle_api_key=$(encrypt_key "$cycle_api_key")
  cycle_hub_id=$(encrypt_key "$cycle_hub_id")
  container_id=$(encrypt_key "$container_id")
  password=$(encrypt_key "$password")
  project_passcode=$(encrypt_key "$project_passcode")

  init_file=$(jq -n \
    --arg cak "$cycle_api_key" \
    --arg ci "$container_id" \
    --arg chi "$cycle_hub_id" \
    --arg pw "$password" \
    --arg usr "$username" \
    --arg ppc "$project_passcode" \
    '{ cycle_api_key: $cak, container_id: $ci, cycle_hub_id: $chi, username: $usr, password: $pw, passcode: $ppc }'
)

echo $init_file | jq  > ./.bicycle

git_ignore_file=$(cat ./.gitignore)

if [[ "$git_ignore_file" != *".bicycle"* ]]
then
  printf "\n.bicycle" >> ./.gitignore
fi

if [ "$use_global" != "y" ] && [ "$use_global" != "yes" ]; then
  #Ask if user wants to store the keys globally.
  echo Do you want to store these credentials globally for use with other projects? [ y/n ]
  read use_global
  if [ "$use_global" = "y" ] || [ "$use_global" = "yes" ]; then
    echo Name the global credentials:
    read global_cred_name
    global_init_object=$(jq -n \
      --arg gcn "$global_cred_name" \
      --arg cak "$cycle_api_key" \
      --arg chi "$cycle_hub_id" \
      --arg pc "$project_passcode" \
      '{ global_cred_name: $gcn, cycle_api_key: $cak, cycle_hub_id: $chi, passcode: $pc }'
    )

    #Check if global bicycle directory and add the creds
    if test -r ~/.bicycle/.bicycle; then
      current_creds=$(cat ~/.bicycle/.bicycle)
      added_creds=$(echo "$current_creds" | jq '. += ['"$global_init_object"']')
      #Save Encrypted Global Passcode
      echo "$added_creds" | jq > ~/.bicycle/.bicycle
    else
      #Make directory in home for global passcode
      mkdir -p ~/.bicycle
      new_array=$(echo [] | jq)
      added_creds=$(echo "$new_array" | jq '. += ['"$global_init_object"']')
      #Save Encrypted Global Passcode
      echo "$added_creds" | jq > ~/.bicycle/.bicycle
    fi
  else
    echo Not saving global credentials...
  fi
fi

echo
echo The init file, .bicycle, was created and added to this directory.
echo The .gitignore file was also updated to include the new init file.
}
