#!/bin/bash

create_global_init() {
  echo Setting global passcode...
  echo

  #Name Global Creds
  echo Name the global credentials:
  read global_cred_name
  if [ -e "$global_cred_name" ]; then 
    exit 
  fi

  #Set Project Passcode
  set_global_passcode
  
  #Get Cycle Api Key
  get_cycle_api_key

  #Get Hub ID
  get_cycle_hub_id


  #Encrypt keys before saving
  cycle_api_key=$(encrypt_key "$cycle_api_key")
  cycle_hub_id=$(encrypt_key "$cycle_hub_id")
  global_passcode=$(encrypt_key "$project_passcode")

  global_init_object=$(jq -n \
    --arg gcn "$global_cred_name" \
    --arg cak "$cycle_api_key" \
    --arg chi "$cycle_hub_id" \
    --arg pc "$global_passcode" \
    '{ global_cred_name: $gcn, cycle_api_key: $cak, cycle_hub_id: $chi, passcode: $pc }'
  )
    
    #Check if global bicycle directory and add the creds
    if test -r ~/.bicycle/.bicycle; then
      current_creds=$(cat ~/.bicycle/.bicycle)
      added_creds=$(echo "$current_creds" | jq '. += ['"$global_init_object"']')
      #Save Encrypted Global Passcode
      echo "$added_creds" | jq > ~/.bicycle/.bicycle
    else
      #Make directory in home for global passcode
      mkdir -p ~/.bicycle
      new_array=$(echo [] | jq)
      added_creds=$(echo "$new_array" | jq '. += ['"$global_init_object"']')
      #Save Encrypted Global Passcode
      echo "$added_creds" | jq > ~/.bicycle/.bicycle
    fi

  echo Global passcode set.
}
