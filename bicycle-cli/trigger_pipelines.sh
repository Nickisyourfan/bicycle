#!/bin/bash

trigger_pipeline_by_choice() {
  require_project_vars

  list_pipeline_names

  choose_pipeline

  pipeline_id=$(echo "$pipelines" | jq '.['"$pipeline_choice"']' | jq -r '.id')
  
  trigger_pipeline

  track_current_job 

  echo "Pipeline triggered!"
}

list_pipeline_names() {
  if [ -e "$cycle_api_key" ]; then
    require_project_vars
  fi
  list_pipelines

  pipelines_count=$(echo "$pipelines" | jq length)

  if [ "$pipelines_count" = 0 ]; then 
    echo You do not have any pipelines.
    echo
    exit
  fi
  for ((i = 0 ; i < "$pipelines_count" ; i++)); do
    pipeline_name=$(echo "$pipelines" | jq '.['"$i"']' | jq '.name')
    echo "$i". "$pipeline_name"
  done
}

choose_pipeline() {
  echo Choose the pipeline: [ by number ]
  read pipeline_choice
}
