#!/bin/bash

fmt_help="  %-20s\t%-54s\n"

announce_formatted() {
  list_annoucements

  number_of_announcemenets=$(echo "$announcements" | jq length)

  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
  for((i = 0 ; i < "$number_of_announcemenets" ; i++)); do
    title=$(echo "$announcements" | jq -r '.['"$i"'].title')
    description=$(echo "$announcements" | jq -r '.['"$i"'].description')
    echo
    echo "$i". "$title"
    echo
    echo "$description"
    echo
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
  done
}
