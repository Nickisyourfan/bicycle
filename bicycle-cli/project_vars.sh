#!/bin/bash

get_project_vars() {
  #Get Project Vars from init file
  if test -r .bicycle; then 
    project_vars=$(cat ./.bicycle) || exit
  fi

  #Map the project vars to make them available.
  eval "$(jq -r '
    "cycle_hub_id=\(.cycle_hub_id|@sh);
    container_id=\(.container_id|@sh);
    cycle_api_key=\(.cycle_api_key|@sh);
    username=\(.username|@sh);
    password=\(.password|@sh);
    passcode=\(.passcode|@sh);"
    ' <<< "$project_vars")"

  #Decrypt API Keys
  if [ "$project_vars" ]; then
    passcode=$(echo "$passcode" | openssl enc -aes-256-cbc -a -d -salt -iter 29 -pass pass:"$project_passcode")
    
    if [[ "$passcode" != "$project_passcode" ]]; then
      echo Wrong passcode... exiting.
      exit
    fi

    cycle_hub_id=$(echo "$cycle_hub_id" | openssl enc -aes-256-cbc -a -d -salt -iter 29 -pass pass:"$project_passcode")
    cycle_api_key=$(echo "$cycle_api_key" | openssl enc -aes-256-cbc -a -d -salt -iter 29 -pass pass:"$project_passcode")
    container_id=$(echo "$container_id" | openssl enc -aes-256-cbc -a -d -salt -iter 29 -pass pass:"$project_passcode")
    password=$(echo "$password" | openssl enc -aes-256-cbc -a -d -salt -iter 29 -pass pass:"$project_passcode")
  fi
}


#Check variables
validate_vars() {
  vars_to_check=("$@")
  for i in "${vars_to_check[@]}"; do 
    if [ -z "$i" ] || [ "$i" = null ]; then
      echo Missing variable. Exiting...
      exit
    fi
  done
}

get_global_vars() {
  if test -r ~/.bicycle/.bicycle; then 
    global_vars=$(cat ~/.bicycle/.bicycle) || exit

    global_var=$(echo "$global_vars" | jq '.['"$global_cred_choice"']')

    eval "$(jq -r '
      "cycle_hub_id=\(.cycle_hub_id|@sh);
      cycle_api_key=\(.cycle_api_key|@sh);
      passcode=\(.passcode|@sh);"
      ' <<< "$global_var")"

    passcode=$(echo "$passcode" | openssl enc -aes-256-cbc -a -d -salt -iter 29 -pass pass:"$project_passcode")
    
    if [[ "$passcode" != "$project_passcode" ]]; then
      echo Wrong passcode... exiting.
      exit
    fi
    
    cycle_hub_id=$(echo "$cycle_hub_id" | openssl enc -aes-256-cbc -a -d -salt -iter 29 -pass pass:"$project_passcode")
    cycle_api_key=$(echo "$cycle_api_key" | openssl enc -aes-256-cbc -a -d -salt -iter 29 -pass pass:"$project_passcode")

  fi
}
