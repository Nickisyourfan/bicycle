#!/bin/bash

get_user_registry_credentials() {
  if [ ! "$username" ]; then
    new_username=true
  fi

  if [ ! "$password" ]; then
    new_password=true
  fi
  #Capture user's registry credentials
  log_in() {
    echo
    echo Docker Hub/Registry Username:
    read username
    echo
    eval_login
  }

  get_pass(){
    echo
    echo Docker Hub Password or Registry Password:
    read -s password
    eval_pass
  }

  eval_pass() {
    if [ -z "$password" ]; then
      echo Password is required.
      exit
    fi
  }
  eval_pass(){
    if [ -z "$password" ] && [ -n "$new_password" ]; then
      get_pass
    elif [ -n "$password" ] && [ -z "$new_password" ]; then
      echo Registry/Hub password is currently set. Set new password? [ y/n ]
      read new_password
      if [ "$new_password" = "yes" ] || [ "$new_password" = "y" ];
      then
        get_pass
      else
        echo Keeping the previous password...
        echo
      fi
    fi
    echo
  }

  eval_login(){
    if [ -z "$username" ] && [ -n "$new_username" ]; then
      echo Setting a new username...
      log_in
    elif [ -n "$username" ] && [ -z "$new_username" ]; then
      echo Registry/Hub username is currently set. Set new username? [ y/n ]
      read new_username
      if [ "$new_username" = "yes" ] || [ "$new_username" = "y" ];
      then
        log_in
      else
        echo Keeping the previous username...
        echo
        eval_pass
      fi
    elif [ -n "$username" ] && [ -n "$new_username" ]; then 
      eval_pass
    fi
    echo
  }
  eval_login
}

docker_login() {
  sudo docker logout
  echo Logging into "$source_type"
  if [ "$source_type" = "docker-registry" ];
  then
    sudo docker login "$source_location" -u "$username" -p "$password"
  else
    sudo docker login -u "$username" -p "$password"
  fi
}

set_project_passcode() {
  if test -e .bicycle; then
    require_project_vars
  else
    if test -e ~/.bicycle/.bicycle; then
      echo Use global credentials and passcode for this project? [ y/n ]
      read use_global
      echo
      case "$use_global" in 
        "yes" | "y")
          echo Using global credentials...
          echo
          choose_global_creds
          get_global_creds
          get_global_vars
          ;;
        *)
          echo Creating custom init...
          echo Set project passcode to encrypt keys:
          read -s project_passcode
          echo
          ;;
      esac
    else
      echo Set project passcode to encrypt keys:
      read -s project_passcode
      echo
    fi
  fi
}

set_global_passcode() {
  echo Set global passcode to encrypt keys:
  read -s project_passcode
  echo
}

#Call this function to make it required to have the project variables.
require_project_vars(){
    echo Enter project passcode:
    read -s project_passcode
    get_project_vars
    echo
}

choose_global_creds() {
  if test -r ~/.bicycle/.bicycle; then 
    global_vars=$(cat ~/.bicycle/.bicycle) || exit
  
    number_of_global_creds=$(echo "$global_vars" | jq length )
    
    
    echo Global Creds:
    for((i = 0 ; i < "$number_of_global_creds" ; i++)); do
      global_name=$(echo "$global_vars" | jq '.['"$i"']' | jq '.global_cred_name')
      echo "$i". "$global_name"
    done
    echo

    echo Choose which global creds to use [Number]:
    read global_cred_choice
  fi
}

get_global_creds(){
    echo Enter global bicycle passcode for these credentials:
    read -s project_passcode
    echo
}
