#!/bin/bash

view_instance_console() {

  #Require projevt variables for this task.
  require_project_vars

  #Get all instances and number of instances
  echo starting getting instances
  list_instances

  #Prompt user to choose the instance to ssh into.
  echo Instances available: "$instance_count"
  get_instance_choice

  #Get credentials for websocket
  get_console_credentials

  #Connect to console api
  request_console_websocket

}
