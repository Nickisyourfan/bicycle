#!/bin/bash

deploy_to_cycle() {
 
  #Project variables are required for this task
  require_project_vars

  #Get the image_id from the container
  fetch_container

  #Get the source_id and source_location from the Image
  fetch_image

  #Log into correct docker account
  echo
  echo Logging into the correct docker account...
  docker_login

  #Check for Prod Dockerfile
  if test -r ./Dockerfile.prod; then 
    echo Do you want to use Dockerfile.prod to build?
    read use_prod
  fi
  #If Prod file, and user says yes, then use prod file.
  if [ "$use_prod" = 'y' ] || [ "$use_prod" = "yes" ]; then
    docker_file="Dockerfile.prod"
  else
    docker_file="Dockerfile"
  fi

  #Build The Image
  echo Building new image...
  #sudo docker build -t "$source_location"'/'"$source_target" . -f "$docker_file"
  sudo docker build -t "$source_target" . -f "$docker_file"

  #Push Image to Docker Hub or Registry
  echo Pushing image to Docker...
  sudo docker push "$source_target" 

  #Create a New Image
  create_image

  #Import the new image
  import_image
  
  #Track The Image Uploading
  track_current_job

  #Reimage The Container
  reimage_container
  
  #Track Reimaging process
  track_current_job

  #Get URL of Deploy
  echo Domain: http://"$container_domain"
  domains_count=$(echo "$domains" | jq length)
  for((i = 0 ; i < "$domains_count" ; i++)); do 
    domain=$(echo "$domains" | jq '.['"$i"']' | jq '.fqdn')
    echo "$i". "$domain"
  done
  echo Done!
}
